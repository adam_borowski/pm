from django.conf.urls import patterns, url
from project.views import ToDoListView, ToDoCreateView, ToDoDeleteView, ToDoUpdateView, TaskCreateView, TaskDeleteView


urlpatterns = patterns('',
    url (
           regex = '^$',
           view =  ToDoListView.as_view(),
           name = 'todo_list'
       ),
    url (
           regex = '^create/$',
           view =  ToDoCreateView.as_view(),
           name = 'todo_create'
       ),
    url (
           regex = '^delete/(?P<pk>\d+)/$',
           view =  ToDoDeleteView.as_view(),
           name = 'todo_delete'
       ),
    url (
           regex = '^update/(?P<pk>\d+)/$',
           view =  ToDoUpdateView.as_view(),
           name = 'todo_update'
       ),
    url (
           regex = '^(?P<todo>\d+)/task/create/$',
           view =  TaskCreateView.as_view(),
           name = 'task_create'
       ),
    url (
           regex = '^(?P<todo>\d+)/task/delete/(?P<pk>\d+)/$',
           view =  TaskDeleteView.as_view(),
           name = 'task_delete'
       ),
)