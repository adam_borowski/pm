from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView, FormView

from .forms import ToDoForm, TaskForm
from .models import ToDo, Task


class LoginView(FormView):
    form_class = AuthenticationForm
    template_name = 'project/login.html'
    
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(LoginView, self).dispatch(*args, **kwargs)
    
    def get_success_url(self):
        return reverse('project:todo_list')
    
    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(LoginView, self).form_valid(form)
    

class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class ToDoMixin(LoginRequiredMixin):
    model = ToDo
    form_class = ToDoForm
    
    def get_success_url(self):
        return reverse('project:todo_list')
    
    def get_queryset(self):
        #return Task.objects.filter(todo__project=self.kwargs['project']).select_related('todo')
        return ToDo.objects.filter(created_by=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(ToDoMixin, self).get_context_data(**kwargs)
        return context
    
    def form_valid(self, form):
        self.todo_list = form.save(commit=False)
        self.todo_list.created_by = self.request.user
        self.todo_list.save()
        return super(ToDoMixin, self).form_valid(form)


class ToDoListView(ToDoMixin, ListView):
    pass


class ToDoDetailView(ToDoMixin, DetailView):
    pass


class ToDoCreateView(ToDoMixin, CreateView):
    pass


class ToDoDeleteView(ToDoMixin, DeleteView):
    pass


class ToDoUpdateView(ToDoMixin, UpdateView):
    pass


class TaskMixin(LoginRequiredMixin):
    model = Task
    form_class = TaskForm
    
    def get_success_url(self):
        return reverse('project:todo_list')
    
    def get_queryset(self):
        return Task.objects.filter(todo=self.kwargs['todo'], todo__created_by=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(TaskMixin, self).get_context_data(**kwargs)
        return context
    
    def form_valid(self, form):
        self.task = form.save(commit=False)
        self.task.created_by = self.request.user
        self.task.todo = get_object_or_404(ToDo, pk=self.kwargs['todo'])
        self.task.save()
        return super(TaskMixin, self).form_valid(form)


class TaskCreateView(TaskMixin, CreateView):
    pass


class TaskDeleteView(TaskMixin, DeleteView):
    pass
