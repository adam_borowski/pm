from django.contrib.auth.models import User
from django.db import models


class ToDo(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)
    created_by = models.ForeignKey(User)
    name = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.name
    
    
class Task(models.Model):
    STATUS_CHOICES = (
        ('R', 'RESOLVED'),
        ('O', 'OPEN'),
    )
    
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)
    todo = models.ForeignKey(ToDo)
    name = models.CharField(max_length=255)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES)
    
    def __unicode__(self):
        return self.name
