from django.forms import ModelForm

from .models import ToDo, Task
        

class ToDoForm(ModelForm):
    class Meta:
        model = ToDo
        exclude = ('created_by',)
        
    
class TaskForm(ModelForm):
    class Meta:
        model = Task
        exclude = ('todo', 'created_by', 'status')