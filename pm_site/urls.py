from django.conf.urls import patterns, include, url
from django.contrib import admin

from project.views import LoginView

admin.autodiscover()

urlpatterns = patterns('',
    url(
        regex = '^accounts/login/$', 
        view = LoginView.as_view(),
        name = 'login'
    ),
    url(
        regex = '^accounts/logout/$', 
        view = 'django.contrib.auth.views.logout', 
        kwargs = {'next_page': '/accounts/login/'},
        name = 'logout'
    ),
    url(
        regex = '^todo/',
        view = include('project.urls', namespace='project', app_name='project')
    ),
    url(
        regex = '^admin/', 
        view = include(admin.site.urls)
    ),
)
